﻿using UnityEngine;
using System.Collections;
using UniRx;
using UniRx.Triggers;
/// <summary>
/// 端末のジャイロセンサーを使って端末の反転させる機能
/// </summary>
public class acceleration : MonoBehaviour {
    Transform maincamera;
    public IObservable<Unit> gyrorotation;
    public float i =1f;

    void Start() {
        i = 1f;
        maincamera = GameObject.Find("Main Camera").GetComponent<Transform>( );
        Input.gyro.enabled = true;
    }
    void Update() {
        Physics.gravity = new Vector3(i * Input.gyro.gravity.x , Physics.gravity.y , Physics.gravity.z);
    }
    public void Turn() {
        gyrorotation = this.UpdateAsObservable( ).Where(cameraRotate => maincamera.localRotation.z > 60);
        this.UpdateAsObservable( ).TakeUntil(gyrorotation).Subscribe(_ => {
            Quaternion gyro = Input.gyro.attitude;
            maincamera.localRotation = ( new Quaternion(0 , 0 , gyro.z , gyro.w) );
        });
    }

}
